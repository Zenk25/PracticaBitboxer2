----------------------------ROLES--------------------------------
insert into role values(1,'ADMIN', 0);
insert into role values(2,'USER', 1);


----------------------------USERS--------------------------------
insert into user values(1, '$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.' ,'admin', 1);
insert into user values(2,'$2a$10$XURPShQNCsLjp1ESc2laoObo9QZDhxz73hJPaEv7/cBha4pk0AgP.', 'user',2);

---------------------------ITEMS--------------------------------
insert into item (IDITEM, STATE, CREATIONDATE, CREATOR, DESCRIPTION ,ITEMCODE , PRICE) values(1,0,'2020-02-05','user','Silla', 'AR-0001', 300.89);
insert into item (IDITEM, STATE, CREATIONDATE, CREATOR, DESCRIPTION ,ITEMCODE , PRICE)  values(2,0,'2020-08-26','user','Sillón', 'AR-0002', 40.89);
insert into item (IDITEM, STATE, CREATIONDATE, CREATOR, DESCRIPTION ,ITEMCODE , PRICE)  values(3,0,'2020-01-11','user','Mesa', 'AR-0003', 730.89);
insert into item (IDITEM, STATE,MESSAGEDISABLED, CREATIONDATE, CREATOR, DESCRIPTION ,ITEMCODE , PRICE)  values(4,1,'Item Disabled','2020-06-08','user','Manta', 'AR-0004', 39.89);
insert into item (IDITEM, STATE, CREATIONDATE, CREATOR, DESCRIPTION ,ITEMCODE , PRICE)  values(5,0,'2020-04-15','user','Estante', 'AR-0005', 63.89);


---------------------------SUPPLIERS--------------------------------
insert into supplier (IDSUPPLIER, SUPPLIERCODE, COUNTRY, NAME) values(1,'SP-0001','SPAIN', 'DESATRANQUES EL BREAN');
insert into supplier (IDSUPPLIER, SUPPLIERCODE, COUNTRY, NAME) values(2,'SP-0002','SWITZERLAND', 'KNIFECUTTERS');
insert into supplier (IDSUPPLIER, SUPPLIERCODE, COUNTRY, NAME) values(3,'SP-0003','TURKEY', 'PRURUR');
insert into supplier (IDSUPPLIER, SUPPLIERCODE, COUNTRY, NAME) values(4,'SP-0004','JAPAN', 'VOCALOID');
insert into supplier (IDSUPPLIER, SUPPLIERCODE, COUNTRY, NAME) values(5,'SP-0005','FRANCE', 'PRESIDENT');


---------------------------ITEMS_SUPLIERS--------------------------------
insert into items_suppliers values(1,1);
insert into items_suppliers values(2,3);
insert into items_suppliers values(3,1);
insert into items_suppliers values(4,2);
insert into items_suppliers values(2,4);


---------------------------PRICEREDUCTIONS--------------------------------
insert into price_reduction values(1,'2020-05-10','PR-0001', 10.0,'2020-04-10', 1);
insert into price_reduction values(2,'2020-05-10','PR-0002', 20.0,'2020-04-10', 2);
insert into price_reduction values(3,'2020-06-01','PR-0003', 10.0,'2020-05-15', 3);
insert into price_reduction values(4,'2020-03-10','PR-0004', 10.0,'2020-01-10', 3);
insert into price_reduction values(5,'2020-08-11','PR-0005', 10.0,'2020-07-11', 5);
