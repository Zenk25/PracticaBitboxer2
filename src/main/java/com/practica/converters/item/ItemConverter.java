package com.practica.converters.item;

import com.practica.dto.ItemDto;
import com.practica.models.Item;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;


public class ItemConverter implements Converter<Item, ItemDto> {

    @Override
    public ItemDto convert(Item source) {
        ModelMapper modelMapper = new ModelMapper();
        ItemDto result = modelMapper.map(source, ItemDto.class);
        return result;
    }
}
