package com.practica.converters.item;

import com.practica.dto.ItemDto;
import com.practica.models.Item;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class ItemDtoConverter implements Converter<ItemDto, Item> {
    @Override
    public Item convert(ItemDto source) {
        ModelMapper modelMapper = new ModelMapper();
        Item result = modelMapper.map(source, Item.class);
        return result;
    }
}
