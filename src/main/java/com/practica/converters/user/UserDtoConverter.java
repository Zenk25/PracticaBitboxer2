package com.practica.converters.user;

import com.practica.dto.ItemDto;
import com.practica.dto.UserDto;
import com.practica.models.Item;
import com.practica.models.User;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class UserDtoConverter implements Converter<UserDto, User> {
    @Override
    public User convert(UserDto source) {
        ModelMapper modelMapper = new ModelMapper();
        User result = modelMapper.map(source, User.class);
        return result;
    }
}
