package com.practica.converters.user;

import com.practica.dto.ItemDto;
import com.practica.dto.UserDto;
import com.practica.models.Item;
import com.practica.models.User;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;


public class UserConverter implements Converter<User, UserDto> {

    @Override
    public UserDto convert(User source) {
        ModelMapper modelMapper = new ModelMapper();
        UserDto result = modelMapper.map(source, UserDto.class);
        return result;
    }
}
