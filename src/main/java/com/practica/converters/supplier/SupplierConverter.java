package com.practica.converters.supplier;

import com.practica.dto.ItemDto;
import com.practica.dto.SupplierDto;
import com.practica.models.Item;
import com.practica.models.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;


public class SupplierConverter implements Converter<Supplier, SupplierDto> {

    @Override
    public SupplierDto convert(Supplier source) {
        ModelMapper modelMapper = new ModelMapper();
        SupplierDto result = modelMapper.map(source, SupplierDto.class);
        return result;
    }
}
