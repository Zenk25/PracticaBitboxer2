package com.practica.converters.supplier;

import com.practica.dto.ItemDto;
import com.practica.dto.SupplierDto;
import com.practica.models.Item;
import com.practica.models.Supplier;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class SupplierDtoConverter implements Converter<SupplierDto, Supplier> {
    @Override
    public Supplier convert(SupplierDto source) {
        ModelMapper modelMapper = new ModelMapper();
        Supplier result = modelMapper.map(source, Supplier.class);
        return result;
    }
}
