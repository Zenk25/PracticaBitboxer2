package com.practica.converters.pricereduction;


import com.practica.dto.PriceReductionsDto;
import com.practica.models.PriceReduction;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class PriceReductionDtoConverter implements Converter<PriceReductionsDto, PriceReduction> {
    @Override
    public PriceReduction convert(PriceReductionsDto source) {
        ModelMapper modelMapper = new ModelMapper();
        PriceReduction result = modelMapper.map(source, PriceReduction.class);
        return result;
    }
}
