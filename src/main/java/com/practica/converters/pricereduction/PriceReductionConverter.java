package com.practica.converters.pricereduction;

import com.practica.dto.PriceReductionsDto;
import com.practica.models.PriceReduction;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;


public class PriceReductionConverter implements Converter<PriceReduction, PriceReductionsDto> {

    @Override
    public PriceReductionsDto convert(PriceReduction source) {
        ModelMapper modelMapper = new ModelMapper();
        PriceReductionsDto result = modelMapper.map(source, PriceReductionsDto.class);
        return result;
    }
}
