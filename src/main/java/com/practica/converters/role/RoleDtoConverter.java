package com.practica.converters.role;

import com.practica.dto.ItemDto;
import com.practica.dto.RoleDto;
import com.practica.models.Item;
import com.practica.models.Role;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;

public class RoleDtoConverter implements Converter<RoleDto, Role> {
    @Override
    public Role convert(RoleDto source) {
        ModelMapper modelMapper = new ModelMapper();
        Role result = modelMapper.map(source, Role.class);
        return result;
    }
}
