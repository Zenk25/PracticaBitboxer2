package com.practica.converters.role;

import com.practica.dto.RoleDto;
import com.practica.models.Role;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;


public class RoleConverter implements Converter<Role, RoleDto> {

    @Override
    public RoleDto convert(Role source) {
        ModelMapper modelMapper = new ModelMapper();
        RoleDto result = modelMapper.map(source, RoleDto.class);
        return result;
    }
}
