package com.practica.services.pricereduction;

import com.practica.dto.PriceReductionsDto;

import java.util.List;

public interface PriceReductionService {

    public PriceReductionsDto getPriceReduction();
    public void addPriceReduction(PriceReductionsDto priceReductionsDto, String itemCode);
    public List<PriceReductionsDto> getAllPriceReductionsByItem();

}
