package com.practica.services.supplier.impl;

import com.practica.dao.item.ItemRepository;
import com.practica.dao.supplier.SupplierRepository;
import com.practica.dto.SupplierDto;
import com.practica.models.Supplier;
import com.practica.services.supplier.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    @Qualifier("mvcConversionService")
    ConversionService conversionService;

    @Override
    public List<SupplierDto> getSuppliers() {
        List<SupplierDto> supplierDtos = new ArrayList<>();
        for (Supplier supplier : supplierRepository.findAll()) {
            supplierDtos.add(conversionService.convert(supplier, SupplierDto.class));
        }
        return supplierDtos;
    }

    @Override
    public void addSupplier(String itemCode, SupplierDto supplierDto) {

    }
}
