package com.practica.services.supplier;

import com.practica.dto.SupplierDto;

import java.util.List;

public interface SupplierService {

    public List<SupplierDto> getSuppliers();
    public void addSupplier(String itemCode, SupplierDto supplierDto);

}
