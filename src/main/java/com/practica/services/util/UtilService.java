package com.practica.services.util;

public interface UtilService {

    public String generateCode(String object);
    public String generateNextCode(String code);
}
