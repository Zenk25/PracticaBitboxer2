package com.practica.services.util.impl;

import com.practica.dao.item.ItemRepository;
import com.practica.dao.pricereduction.PriceReductionRepository;
import com.practica.dao.supplier.SupplierRepository;
import com.practica.services.util.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UtilServiceImpl implements UtilService {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PriceReductionRepository priceReductionRepository;

    @Override
    public String generateCode(String object) {
        switch(object.toUpperCase()){
            case "ITEM":
                String code = itemRepository.findFirst1ByOrderByIdItemDesc().getItemCode();
                String codeArray[] = code.split("-");
                Long value = Long.valueOf(codeArray[1]) + 1;
                String aux = "";
                for (int i = 0; i <= 4 - String.valueOf(value).length()-1; i++) {
                    aux += "0";
                }
                aux += String.valueOf(value);
                codeArray[1] = aux;
                code = codeArray[0] + "-" + codeArray[1];

                return code;
            case "PRICEREDUCTION":
                code = priceReductionRepository.findFirst1ByOrderByIdPriceReductionDesc().getPriceReductionCode();
                codeArray = code.split("-");
                value = Long.valueOf(codeArray[1]) + 1;
                aux = "";
                for (int i = 0; i <= 4 - String.valueOf(value).length()-1; i++) {
                    aux += "0";
                }
                aux += String.valueOf(value);
                codeArray[1] = aux;
                code = codeArray[0] + "-" + codeArray[1];

                return code;
            case "SUPPLIER":
                code = supplierRepository.findFirst1ByOrderByIdSupplierDesc().getSupplierCode();
                codeArray = code.split("-");
                value = Long.valueOf(codeArray[1]) + 1;
                aux = "";
                for (int i = 0; i <= 4 - String.valueOf(value).length()-1; i++) {
                    aux += "0";
                }
                aux += String.valueOf(value);
                codeArray[1] = aux;
                code = codeArray[0] + "-" + codeArray[1];

                return code;
            default:
        }
        return null;
    }

    @Override
    public String generateNextCode(String code) {
        String codeArray[] = code.split("-");
        Long value = Long.valueOf(codeArray[1]) + 1;
        String aux = "";
        for (int i = 0; i <= 4 - String.valueOf(value).length()-1; i++) {
            aux += "0";
        }
        aux += String.valueOf(value);
        codeArray[1] = aux;
        code = codeArray[0] + "-" + codeArray[1];

        return code;
    }
}
