package com.practica.services.item;

import com.practica.dto.ItemDto;

import java.util.List;


public interface ItemService {

    public ItemDto getItem(String itemCode);
    //TODO implementar que la creationDate sea la actual
    public ItemDto addItem(ItemDto itemDto);
    public ItemDto updateItem(String itemCode, ItemDto itemDto);
    public ItemDto updateItemState(String itemCode, ItemDto itemDto);
    public String deleteItem(String itemCode);
    public List<ItemDto> getAllItems();

}
