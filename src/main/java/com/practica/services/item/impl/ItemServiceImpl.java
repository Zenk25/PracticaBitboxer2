package com.practica.services.item.impl;

import com.practica.dao.item.ItemRepository;
import com.practica.dao.pricereduction.PriceReductionRepository;
import com.practica.dao.supplier.SupplierRepository;
import com.practica.dto.ItemDto;
import com.practica.dto.PriceReductionsDto;
import com.practica.dto.SupplierDto;
import com.practica.enums.StateEnum;
import com.practica.models.Item;
import com.practica.models.PriceReduction;
import com.practica.models.Supplier;
import com.practica.services.item.ItemService;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Autowired
    SupplierRepository supplierRepository;

    @Autowired
    PriceReductionRepository priceReductionRepository;

    @Autowired
    @Qualifier("mvcConversionService")
    ConversionService conversionService;

    @Override
    public ItemDto getItem(String itemCode) {
        Item item;
        ItemDto itemDto = new ItemDto();
        Optional<Item> value = itemRepository.findByItemCode(itemCode);
        if (value.isPresent()) {
            item = value.get();
            itemDto = conversionService.convert(item, ItemDto.class);
        }
        return itemDto;
    }

    @Override
    public ItemDto addItem(ItemDto itemDto) {
        Item item = conversionService.convert(itemDto, Item.class);
        item.setCreationDate(new Date());
        try {
            List<Supplier> suppliers = new ArrayList<>();
            if (itemDto.getSuppliers() != null) {
                for (SupplierDto supplierDto : itemDto.getSuppliers()) {
                    if(supplierRepository.findBysupplierCode(supplierDto.getSupplierCode()).isPresent()) {
                        suppliers.add(supplierRepository.findBysupplierCode(supplierDto.getSupplierCode()).get());
                    }else{
                        //TODO Exception no existe el Supplier.
                        return null;
                    }
                }

            }

            item.setSuppliers(suppliers);
            if (itemDto.getPriceReductions() != null) {
                if(!dateOverlaps(itemDto.getPriceReductions())){
                    return null;
                }
                for (PriceReductionsDto priceReductionsDto : itemDto.getPriceReductions()) {

                    if(priceReductionRepository.findBypriceReductionCode(priceReductionsDto.getPriceReductionCode()).isPresent()) {
                        //TODO Exception ya existe el codigo de PriceReduction.
                        return null;
                    }
                }
            }
            itemRepository.save(item);
            for (PriceReduction priceReduction : item.getPriceReductions()) {
                priceReduction.setItem(item);
                priceReductionRepository.save(priceReduction);
            }

        } catch (Exception e) {
            //TODO cuando esten las expceptions creadas se quitaran el try/catch
            return null;
        }
        return conversionService.convert(item,ItemDto.class);
    }

    @Override
    public ItemDto updateItem(String itemCode, ItemDto itemDto) {
        Long itemId = itemRepository.findByItemCode(itemCode).get().getIdItem();
        Item item = null;

        if (itemId != null && itemDto != null) {
            item = conversionService.convert(itemDto, Item.class);
            item.setIdItem(itemId);
            List<Supplier> suppliers = new ArrayList<>();
            if (itemDto.getSuppliers() != null) {
                for (SupplierDto supplierDto : itemDto.getSuppliers()) {
                    if(supplierRepository.findBysupplierCode(supplierDto.getSupplierCode()).isPresent()) {
                        suppliers.add(supplierRepository.findBysupplierCode(supplierDto.getSupplierCode()).get());
                    }else{
                        //TODO Exception no existe el Supplier.
                        return null;
                    }
                }

            }

            item.setSuppliers(suppliers);
            List<PriceReduction> priceReductions = new ArrayList<>();
            if (itemDto.getPriceReductions() != null) {
                if(!dateOverlaps(itemDto.getPriceReductions())){
                    return null;
                }
                for (PriceReductionsDto priceReductionsDto : itemDto.getPriceReductions()) {
                    if(priceReductionRepository.findBypriceReductionCode(priceReductionsDto.getPriceReductionCode()).isPresent()) {
                        priceReductions.add(priceReductionRepository.findBypriceReductionCode(priceReductionsDto.getPriceReductionCode()).get());
                    }else{
                        priceReductions.add(conversionService.convert(priceReductionsDto,PriceReduction.class));
                    }
                }
            }
            item.setPriceReductions(priceReductions);
            for (PriceReduction priceReduction : item.getPriceReductions()) {
                priceReduction.setItem(item);
            }
            itemRepository.save(item);
        }
        return conversionService.convert(item, ItemDto.class);
    }

    @Override
    public ItemDto updateItemState(String itemCode, ItemDto itemDto) {
        Item item = itemRepository.findByItemCode(itemCode).get();
        if(itemDto.getStateEnum() == StateEnum.ACTIVE){
            item.setState(StateEnum.DISCONTINUED);
            item.setMessageDisabled(itemDto.getMessageDisabled());
        }else{
            item.setMessageDisabled("");
            item.setState(StateEnum.ACTIVE);
        }
        itemRepository.save(item);
        return conversionService.convert(item,ItemDto.class);
    }

    @Override
    public String deleteItem(String itemCode) {
        Item item = itemRepository.findByItemCode(itemCode)
                .orElseThrow(() -> {
                    return new NoSuchElementException("No se ha encontrado el item");
                });
        for (PriceReduction priceReduction : item.getPriceReductions()) {
            priceReductionRepository.delete(priceReduction);
        }
        itemRepository.delete(item);
        return itemCode;
    }

    @Override
    public List<ItemDto> getAllItems() {
        List<Item> itemList = itemRepository.findAll();
        List<ItemDto> result = new ArrayList<>();
        if (itemList != null) {
            for (Item item : itemList) {
                result.add(conversionService.convert(item, ItemDto.class));
            }
            return result;
        } else {
            return null;
        }
    }


    private boolean dateOverlaps(List<PriceReductionsDto> priceReductions){
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        for (PriceReductionsDto priceReduction: priceReductions) {
            DateTime fechaInicio1 = formatter.parseDateTime(priceReduction.getStartDate().toString());
            DateTime fechaFin1 = formatter.parseDateTime(priceReduction.getEndDate().toString());
            Interval intervalo1 = new Interval( fechaInicio1, fechaFin1 );
            List<PriceReductionsDto> filteredList =
                    priceReductions.stream().filter(p -> !priceReduction
                            .equals(p)).collect(Collectors.toList());
            for (PriceReductionsDto filtered: filteredList) {
                DateTime fechaInicio2 = formatter.parseDateTime(filtered.getStartDate().toString());
                DateTime fechaFin2 = formatter.parseDateTime(filtered.getEndDate().toString());
                Interval intervalo2 = new Interval( fechaInicio2, fechaFin2 );
                if(intervalo1.overlaps( intervalo2 )){
                    return false;
                }
            }
        }
        return true;
    }

}
