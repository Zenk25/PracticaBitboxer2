package com.practica.services.user;

import com.practica.dto.UserDto;
import com.practica.models.User;

import java.util.List;

public interface UserService {

    public UserDto getUser(String name);
    public UserDto addUser(UserDto userDto);
    public UserDto updateUser(String userName, UserDto userDto);
    public String deleteUser(String userName);
    public List<UserDto> getAllUsers();

}
