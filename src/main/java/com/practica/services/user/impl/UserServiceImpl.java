package com.practica.services.user.impl;

import com.practica.dao.user.UserRepository;
import com.practica.dto.UserDto;
import com.practica.models.User;
import com.practica.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    @Qualifier("mvcConversionService")
    ConversionService conversionService;

    @Override
    public UserDto getUser(String userName) {
        return conversionService.convert(userRepository.findByUsername(userName).get(), UserDto.class);
    }

    @Override
    public UserDto addUser(UserDto userDto) {
        return conversionService.convert(userRepository.save(conversionService.convert(userDto, User.class)), UserDto.class);
    }

    @Override
    public UserDto updateUser(String userName, UserDto userDto) {
        return null;
    }

    @Override
    public String deleteUser(String userName) {
        User user = userRepository.findByUsername(userName).get();
        userRepository.delete(user);
        return userName;
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<UserDto> result = new ArrayList<>();
        for (User user: userRepository.findAll()) {
            result.add(conversionService.convert(user,UserDto.class));
        }
        return result;
    }
}
