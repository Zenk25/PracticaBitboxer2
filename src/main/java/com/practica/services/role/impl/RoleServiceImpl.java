package com.practica.services.role.impl;

import com.practica.dao.role.RoleRepository;
import com.practica.dto.RoleDto;
import com.practica.models.Role;
import com.practica.services.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    @Qualifier("mvcConversionService")
    ConversionService conversionService;

    @Override
    public RoleDto getRoleByUser(String userName) {
        return null;
    }

    @Override
    public List<RoleDto> getAllRoles() {
        List<Role> roleList = roleRepository.findAll();
        List<RoleDto> result = new ArrayList<>();
        if (roleList != null) {
            for (Role role : roleList) {
                result.add(conversionService.convert(role, RoleDto.class));
            }
            return result;
        } else {
            return null;
        }    }
}
