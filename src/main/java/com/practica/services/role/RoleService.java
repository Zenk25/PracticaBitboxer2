package com.practica.services.role;

import com.practica.dto.RoleDto;

import java.util.List;

public interface RoleService {

    public RoleDto getRoleByUser(String userName);
    public List<RoleDto> getAllRoles();
}
