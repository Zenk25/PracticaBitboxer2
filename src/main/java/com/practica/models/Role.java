package com.practica.models;

import com.practica.enums.RoleEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "ROLE")
@Data
public class Role {

    @Id
    @Column(name = "IDROLE")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "role_sequence")
    @SequenceGenerator(name = "role_sequence", initialValue = 3, allocationSize = 100)
    private Long idRole;

    @Column(name = "ROLE")
    private RoleEnum role;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role")
    private List<User> users;

}
