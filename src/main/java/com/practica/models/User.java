package com.practica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name="USER")
@Getter
@Setter
public class User {

    @Id
    @Column(name = "IDUSER")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence", initialValue = 3, allocationSize = 100)
    private Long idUser;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @ManyToOne(cascade = CascadeType.ALL)
    private Role role;

    public User() {
    }

    public User(String name, String password, Role role) {
        this.username = name;
        this.password = password;
        this.role = role;
    }

}
