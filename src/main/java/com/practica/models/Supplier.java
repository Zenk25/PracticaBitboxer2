package com.practica.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Supplier")
@Getter
@Setter
public class Supplier {

    @Id
    @Column(name = "IDSUPPLIER")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "supplier_sequence")
    @SequenceGenerator(name = "supplier_sequence", initialValue = 6, allocationSize = 100)
    private Long idSupplier;

    @Column(name = "SUPPLIERCODE", unique = true)
    private String supplierCode;

    @Column(name = "NAME")
    private String name;

    @Column(name = "COUNTRY")
    private String country;

    @ManyToMany(mappedBy = "suppliers")
    private List<Item> items;

    public Supplier() {
    }

    public Supplier(String name, String country) {
        this.name = name;
        this.country = country;
    }

}
