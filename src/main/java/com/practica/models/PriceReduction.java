package com.practica.models;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDate;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name="PriceReduction")
@Getter
@Setter
public class PriceReduction {

    @Id
    @Column(name="IDPRICEREDUCTION")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "priceReduction_sequence")
    @SequenceGenerator(name="priceReduction_sequence", initialValue = 6, allocationSize = 100)
    private Long idPriceReduction;

    @Column(name = "PRICEREDUCTIONCODE", unique = true)
    private String priceReductionCode;

    @Column(name = "REDUCEDPRICE")
    private double reducedPrice;

    @Column(name = "STARTDATE")
    private LocalDate startDate;

    @Column(name = "ENDDATE")
    private LocalDate endDate;

    @ManyToOne(cascade = CascadeType.ALL)
    private Item item;


    public PriceReduction() {
    }

    public PriceReduction(double reducedPrice, LocalDate startDate, LocalDate endDate) {
        this.reducedPrice = reducedPrice;
        this.startDate = startDate;
        this.endDate = endDate;
    }


}
