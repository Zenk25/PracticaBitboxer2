package com.practica.models;

import com.practica.enums.StateEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JoinColumnOrFormula;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name="ITEM")
@Getter
@Setter
public class Item {

    @Id
    @Column(name="IDITEM")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_sequence")
    @SequenceGenerator(name= "item_sequence", initialValue= 6, allocationSize = 100)
    private Long idItem;


    @Column(name="ITEMCODE", unique = true)
    private String itemCode;

    @Column(name="DESCRIPTION")
    private String description;

    @Column(name="PRICE")
    private double price;

    @Column(name="STATE")
    private StateEnum state;

    @Column(name="MESSAGEDISABLED")
    private String messageDisabled;

    @ManyToMany
    @JoinTable(
            name= "ITEMS_SUPPLIERS",
            joinColumns = @JoinColumn(name="IDITEM"), inverseJoinColumns = @JoinColumn(name="IDSUPPLIER")
    )
    private List<Supplier> suppliers;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "item")
    private List<PriceReduction> priceReductions;

    @Column(name="CREATIONDATE")
    private Date creationDate;

    @Column(name="CREATOR")
    private String creator;

    public Item() {
    }

    public Item(String itemCode, String description, double price, StateEnum stateEnum, List<Supplier> suppliers, List<PriceReduction> priceReductions, Date creationDate, String creator) {
        this.itemCode = itemCode;
        this.description = description;
        this.price = price;
        this.state = stateEnum;
        this.suppliers = suppliers;
        this.priceReductions = priceReductions;
        this.creationDate = creationDate;
        this.creator = creator;
    }


    @Override
    public String toString() {
        return "ItemDto{" +
                "itemCode='" + itemCode + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stateEnum=" + state +
                ", suppliers=" + suppliers +
                ", priceReductions=" + priceReductions +
                ", creationDate=" + creationDate +
                ", creator=" + creator +
                '}';
    }
}
