package com.practica.dao.pricereduction;

import com.practica.models.Item;
import com.practica.models.PriceReduction;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PriceReductionRepository extends JpaRepository<PriceReduction, Long> {

    public Optional<PriceReduction> findBypriceReductionCode(String priceReductionCode);

    public PriceReduction findFirst1ByOrderByIdPriceReductionDesc();
}
