package com.practica.dao.item;

import com.practica.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ItemRepository extends JpaRepository<Item, Long> {

    public Optional<Item> findByItemCode(String itemCode);

    public Item findFirst1ByOrderByIdItemDesc();

    @Query(value ="SELECT * FROM item " +
            "JOIN items_suppliers ON items_suppliers.iditem = item.iditem " +
            "JOIN (" +
            "             SELECT items_suppliers.idsupplier, MIN(item.price) pricemin " +
            "             FROM item " +
            "             JOIN items_suppliers ON items_suppliers.iditem = item.iditem " +
            "             GROUP BY items_suppliers.idsupplier) " +
            "result " +
            "ON result.idsupplier = items_suppliers.idsupplier " +
            "AND result.pricemin = item.price;",
        nativeQuery = true)
    List<Item> sqlQuery1();

    @Query(value ="SELECT * FROM SUPPLIER" +
            "INNER JOIN items_suppliers on items_suppliers.idsupplier = supplier.idsupplier " +
            "inner join item on item.iditem = items_suppliers.iditem " +
            "inner join price_reduction on price_reduction.item_iditem = item.iditem;",
    nativeQuery = true)
    List<Item> sqlQuery2();

}
