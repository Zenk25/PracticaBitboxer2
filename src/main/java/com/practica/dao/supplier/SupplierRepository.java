package com.practica.dao.supplier;

import com.practica.models.Item;
import com.practica.models.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface SupplierRepository extends JpaRepository<Supplier, Long> {


    public Optional<Supplier> findBysupplierCode(String supplierCode);

    public Supplier findFirst1ByOrderByIdSupplierDesc();

}
