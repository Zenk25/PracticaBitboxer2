package com.practica.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.practica.enums.StateEnum;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ItemDto {

    private String itemCode;
    private String description;
    private double price;
    private StateEnum stateEnum;
    private String messageDisabled;
    private List<SupplierDto> suppliers;
    private List<PriceReductionsDto> priceReductions;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Europe/London")
    private Date creationDate;
    private String creator;



    @Override
    public String toString() {
        return "ItemDto{" +
                "itemCode='" + itemCode + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stateEnum=" + stateEnum +
                ", suppliers=" + suppliers +
                ", priceReductions=" + priceReductions +
                ", creationDate=" + creationDate +
                ", creator=" + creator +
                '}';
    }
}

