package com.practica.dto;

import lombok.Data;

@Data
public class SupplierDto {

    private String name;
    private String country;
    private String supplierCode;

}
