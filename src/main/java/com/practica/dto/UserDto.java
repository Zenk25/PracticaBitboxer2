package com.practica.dto;

import com.practica.enums.RoleEnum;
import com.practica.models.Role;
import lombok.Data;

@Data
public class UserDto {

    private String username;
    private String password;
    private String token;
    private RoleDto role;


}
