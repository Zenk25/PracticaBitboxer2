package com.practica.dto;

import com.practica.enums.RoleEnum;
import com.practica.models.User;
import lombok.Data;

import java.util.List;

@Data
public class RoleDto {

    private RoleEnum role;
    private String description;
}
