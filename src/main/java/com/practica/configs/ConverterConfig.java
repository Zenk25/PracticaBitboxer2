package com.practica.configs;

import com.practica.converters.item.ItemConverter;
import com.practica.converters.item.ItemDtoConverter;
import com.practica.converters.pricereduction.PriceReductionConverter;
import com.practica.converters.pricereduction.PriceReductionDtoConverter;
import com.practica.converters.role.RoleConverter;
import com.practica.converters.role.RoleDtoConverter;
import com.practica.converters.supplier.SupplierConverter;
import com.practica.converters.supplier.SupplierDtoConverter;
import com.practica.converters.user.UserConverter;
import com.practica.converters.user.UserDtoConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class ConverterConfig implements WebMvcConfigurer {

    public void addFormatters(FormatterRegistry registry){
        registry.addConverter(new ItemConverter());
        registry.addConverter(new ItemDtoConverter());
        registry.addConverter(new UserConverter());
        registry.addConverter(new UserDtoConverter());
        registry.addConverter(new PriceReductionConverter());
        registry.addConverter(new PriceReductionDtoConverter());
        registry.addConverter(new RoleConverter());
        registry.addConverter(new RoleDtoConverter());
        registry.addConverter(new SupplierConverter());
        registry.addConverter(new SupplierDtoConverter());
    }
}
