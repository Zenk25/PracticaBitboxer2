package com.practica.controllers;


import com.practica.dto.ItemDto;
import com.practica.services.item.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/items")
public class ItemController {

    @Autowired
    ItemService itemService;

    @GetMapping("/findItems")
    public ResponseEntity<Object> getItems(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(itemService.getAllItems(), HttpStatus.OK);
    }
    @GetMapping("/item/{itemCode}")
    public ResponseEntity<Object> getItem(HttpServletRequest request, @PathVariable String itemCode) throws  Exception{
        return new ResponseEntity<Object>(itemService.getItem(itemCode), HttpStatus.OK);
    }
    @PostMapping("/item/save")
    public ResponseEntity<Object> saveItem(HttpServletRequest request, @RequestBody ItemDto item) throws  Exception{
        return new ResponseEntity<Object>(itemService.addItem(item), HttpStatus.OK);
    }
    @PutMapping("/item/update/{itemCode}")
    public ResponseEntity<Object> updateItem(HttpServletRequest request, @RequestBody ItemDto item, @PathVariable String itemCode) throws  Exception{
        return new ResponseEntity<Object>(itemService.updateItem(itemCode, item), HttpStatus.OK);
    }

    @PutMapping("/item/updateState/{itemCode}")
    public ResponseEntity<Object> updateItemState(HttpServletRequest request, @RequestBody ItemDto item, @PathVariable String itemCode) throws  Exception{
        return new ResponseEntity<Object>(itemService.updateItemState(itemCode, item), HttpStatus.OK);
    }

    @DeleteMapping("/item/delete/{itemCode}")
    public ResponseEntity<Object> deleteItem(HttpServletRequest request, @PathVariable String itemCode) throws  Exception{
        return new ResponseEntity<Object>(itemService.deleteItem(itemCode), HttpStatus.OK);
    }

}

