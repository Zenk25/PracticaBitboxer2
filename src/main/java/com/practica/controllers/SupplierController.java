package com.practica.controllers;

import com.practica.services.item.ItemService;
import com.practica.services.supplier.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/suppliers")
public class SupplierController {

    @Autowired
    SupplierService supplierService;

    @GetMapping("/findSuppliers")
    public ResponseEntity<Object> getItems(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(supplierService.getSuppliers(), HttpStatus.OK);
    }
}
