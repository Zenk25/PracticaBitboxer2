package com.practica.controllers;


import com.practica.services.util.UtilService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/util")
public class UtilController {

    @Autowired
    UtilService utilService;

    @GetMapping("/nextCode/{object}")
    public String nextCode(@PathVariable String object){
        return utilService.generateCode(object);
    }

    @GetMapping("/nextCodeGenerator/{code}")
    public String nextCodeGenerator(@PathVariable String code){
        return utilService.generateNextCode(code);
    }
}
