package com.practica.controllers;

import com.practica.dto.ItemDto;
import com.practica.dto.UserDto;
import com.practica.services.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/findUsers")
    public ResponseEntity<Object> getUsers(HttpServletRequest request) throws  Exception{
        return new ResponseEntity<Object>(userService.getAllUsers(), HttpStatus.OK);
    }
    @GetMapping("/get/{userName}")
    public ResponseEntity<Object> getUser(HttpServletRequest request, @PathVariable String userName) throws  Exception{
        return new ResponseEntity<Object>(userService.getUser(userName), HttpStatus.OK);
    }
    @PostMapping("/save")
    public ResponseEntity<Object> saveUser(HttpServletRequest request, @RequestBody UserDto userDto) throws  Exception{
        return new ResponseEntity<Object>(userService.addUser(userDto), HttpStatus.OK);
    }
    @PutMapping("/update/{userName}")
    public ResponseEntity<Object> updateUser(HttpServletRequest request, @RequestBody UserDto userDto, @PathVariable String userName) throws  Exception{
        return new ResponseEntity<Object>(userService.updateUser(userName, userDto), HttpStatus.OK);
    }
    @DeleteMapping("/delete/{userName}")
    public ResponseEntity<Object> deleteUser(HttpServletRequest request, @PathVariable String userName) throws  Exception{
        return new ResponseEntity<Object>(userService.deleteUser(userName), HttpStatus.OK);
    }

}
